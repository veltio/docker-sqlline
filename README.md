# Provides a Docker Image with SQLLine Application

Includes drivers for:

- MySQL
- Firebird
- Vertica

## How to use

Just type `docker run -it veltio/sqlline` then, inside a running container, type:

```
!connect jdbc:firebirdsql://192.168.254.4/d:/banco/sghdados.098 SYSDBA masterkey org.firebirdsql.jdbc.FBDriver
```
or
```
!connect jdbc:vertica://192.168.122.66/sghdw sghdw sghdw com.vertica.jdbc.Driver
```

... and start type your queries. ;)

Para encerrar, digite `!quit`. 